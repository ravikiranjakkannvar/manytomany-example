/**
 * 
 */
package exa.com.springbootdemo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import exa.com.app.HelloController;

/**
 * @author Ravikiran J
 *
 */
@EnableAutoConfiguration // it will auto configure what we used in the classpath
@ComponentScan({"exa.com"}) //  scans for package what we provide
public class HelloControllerTest extends SpringBootDemoApplicationTests {


	@Autowired
	private WebApplicationContext webApplicationContext;
	//Interface to provide configuration for a web application. This is read-only while the application is running, but may be reloaded if the implementation supports this

	private MockMvc mockMvc;

	 @Before
     public void setUp() {
      mockMvc = MockMvcBuilders.standaloneSetup(new HelloController()).build();
     }

	 @Test
     public void testIndex() throws Exception{
            this.mockMvc.perform(get("/"))
                 .andExpect(status().isOk())
                 .andExpect(view().name("welcome"))
                 .andDo(print());
     }

}
