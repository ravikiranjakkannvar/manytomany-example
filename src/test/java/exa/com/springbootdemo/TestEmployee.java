/**
 * 
 */
package exa.com.springbootdemo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import exa.com.app.HelloController;
import exa.com.controllers.RestController;

/**
 * @author Ravikiran J
 *
 */
@EnableAutoConfiguration // it will auto configure what we used in the classpath
@ComponentScan({"exa.com"}) //  scans for package what we provide
public class TestEmployee extends SpringBootDemoApplicationTests {


	@Autowired
	private WebApplicationContext webApplicationContext;
	//Interface to provide configuration for a web application. This is read-only while the application is running, but may be reloaded if the implementation supports this
	
	private MockMvc mockMvc;

	 @Before
     public void setUp() {
      mockMvc = MockMvcBuilders.standaloneSetup(new RestController()).build();
     }

	@Test
	public void testEmployee() throws Exception {
		mockMvc.perform(get("/employee")).andExpect(status().isOk())
		.andExpect(content().contentType("application/json;charset=UTF-8"))
		.andExpect(jsonPath("$.firstname").value("raj")).andExpect(jsonPath("$.lastname").value("kiran"))
		  .andDo(print());
	}

}
