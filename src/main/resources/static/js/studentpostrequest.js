$(document)
.ready(
		function() {

			var url = window.location.origin;

			// SUBMIT FORM
			$("#customerForm").submit(function(event) {
				// Prevent the form from submitting via the browser.
				
				event.preventDefault();
				ajaxPost();
			});

			function ajaxPost() {

				// PREPARE FORM DATA
				var formData = {
						name : $("#name").val(),
						rollNo : $("#rollNo").val(),
						city : $("#city").val()
				}
				alert("ajax calling : " + formData);

				// DO POST
				$
				.ajax({
					type : "POST",
					contentType : "application/json",
					url : url + "/save",
					data : JSON.stringify(formData),
					//data : formData,

					cache: false, // Force requested pages not to be cached by the browser
					dataType : 'json',
					success : function(result) {
						if (result.status == "Done") {

							$("#postResultDiv")
							.html(
									"<strong>"
									+ "Post Successfully! Student's Info: FirstName = "
									+ result.data.name
									+ " ,RollNo = "
									+ result.data.rollNo
									+ " ,City = "
									+ result.data.city
									+ "</strong>");
						} else {
							$("#postResultDiv")
							.html(
							"<strong>Error</strong>");
						}
						console.log(result);
					},
					error : function(e) {
						alert("Error!")
						console.log("ERROR: ", e);
					}
				});

				// Reset FormData after Posting
				resetData();

			}

			function resetData() {
				$("#name").val("");
				$("#rollNo").val("");
				$("#city").val("");
			}
		})
