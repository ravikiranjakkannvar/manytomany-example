/**
 * 
 */
package exa.com.controllers;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import exa.com.models.Course;
import exa.com.models.Response;
import exa.com.models.Student;
import exa.com.services.CourseService;
import exa.com.services.StudentService;

/**
 * @author Ravikiran J
 *
 */
@Controller
public class StudentController {

	//logger declaration
	final static Logger logger = LoggerFactory.getLogger(StudentController.class);

	@Autowired
	private StudentService studentServ;

	@Autowired
	private CourseService courseServ;


	@ModelAttribute("student") 
	public Student getAddress(){
		return new Student();
	}

	@ModelAttribute("course") 
	public Course getCourse(){
		return new Course();
	}

	//show customerPage
	@GetMapping(value="/course-add")
	public String showCourseForm(Model model) {
		logger.info("Customer Page : "+this.getClass());
		
		return "course-add";
	}


	//show customerPage
	@GetMapping(value="/customer-add")
	public String showCustomerForm() {
		logger.info("Customer Page : "+this.getClass());
		return "customer-add";
	}

	//show indexPage
	@GetMapping(value="/")
	public String showform() {
		logger.info("Welcome Page : "+this.getClass());
		return "welcome";
	}

	//show student-add page
	@GetMapping(value="/student-add")
	public String showFormStudent(Model model) {
		/*logger.info("Student-add : "+this.getClass());
		model.addAttribute("students", studentServ.list());*/
		/*model.addAttribute("courses", courseServ.getList());*/
		model.addAttribute("courses", courseServ.getList());
		logger.info("list succes :"+studentServ.list());
		return "student-add";
	}

	//show student-add page
	@GetMapping(value="/student-edit")
	public String showFormStudentEdit(Model model,RedirectAttributes redirectAttributes) {
		return "student-edit";
	}



	//list 
	@GetMapping(value="/student-list")
	@ResponseBody
	public Response showFormStudentList(Model model) {
		logger.info("Student-list : "+this.getClass());
		//model.addAttribute("students", studentServ.list());
		Response response = new Response("Done", studentServ.list());
		return response;
	}

	//save
	@PostMapping(value="/save",produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response saveStudent(@RequestBody Student student,Model model,BindingResult result) {
		if(result.hasErrors()) {
			logger.error("error in save method : "+student.toString()+"and controller is :"+this.getClass());
			/*return student.toString();*/
		}

		Response response=new Response("Done",student );
		logger.info("response :"+response);
		studentServ.save(student);
		logger.info("save succes :"+this.getClass());
		return response;
	}

	//list
	@PostMapping(value="/list")
	public String listStudent(@ModelAttribute("student")Student student,Model model,BindingResult result) {
		if(result.hasErrors()) {
			logger.error("error in save method : "+student.toString()+"and controller is :"+this.getClass());
			return "error";
		}
		model.addAttribute("students", studentServ.list());
		logger.info("list succes :"+studentServ.list());
		return "student-list";
	}

	@GetMapping(value="/edit/{id}")
	public String getOneStudent(@PathVariable("id")int studentId,Model model) {
		Student student=studentServ.findOne(studentId);

		if(student!=null) {
			model.addAttribute("Student",student);
			logger.info("edit page display : "+student.toString());
			return "student-edit";
		}
		logger.info("edit error: "+student.toString());
		return "redirect:/student-edit?status=false";
	}

	@PostMapping("/edit/save")
	public String editSave(@ModelAttribute("student")Student student,Model model,BindingResult result) {

		if(result.hasErrors()) {
			logger.error("error in save edit"+student.toString());
		}
		studentServ.update(student);
		return"redirect:/student-edit?status=true";
	}


	/*many to many save operation*/
	@PostMapping("/student/save")
	public String studentSave(@ModelAttribute("student")Student student,Model model,BindingResult result) {

		if(result.hasErrors()) {
			logger.error("error in save edit"+student.toString());
		}
		studentServ.save(student);
		return"redirect:/student-add?status=true";
	}

	@PostMapping("/course/save")
	public String courseSave(@ModelAttribute("course")Course course,Model model,BindingResult result) {

		if(result.hasErrors()) {
			logger.error("error in save edit"+course.toString());
		}
		courseServ.save(course);
		return"redirect:/course-add?status=true";
	}


}
