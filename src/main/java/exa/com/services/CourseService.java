/**
 * 
 */
package exa.com.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import exa.com.controllers.CourseController;
import exa.com.models.Course;
import exa.com.repositories.CourseRepository;

/**
 * @author Ravikiran J
 *
 */
@Service
@Transactional
public class CourseService {

	Logger logger=LoggerFactory.getLogger(CourseService.class);

	@Autowired
	private CourseRepository repo;

	//list
	public List<Course> getList(){
		return repo.findAll();
	}

	//save
	public Course save(Course course) {
		logger.info("Save method at service passed : "+this.getClass());
		return repo.save(course);
	}
	
	//update
	public Course update(Integer id, Course course) {
		logger.info("update method at service passed : "+this.getClass());
		return repo.save(course);
	}

	//findone
	public Course findOne(Integer id) {
		logger.info("FindOne method at service passed : "+this.getClass());
		return repo.findOne(id);
	}

	//delete
	public void delete(Integer id) {
		logger.info("delete method at service passed : "+this.getClass());
		repo.delete(id);
	}



}
