/**
 * 
 */
package exa.com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import exa.com.models.Course;

/**
 * @author Ravikiran J
 *
 */
@Repository
public interface CourseRepository extends JpaRepository<Course, Integer>{

}
