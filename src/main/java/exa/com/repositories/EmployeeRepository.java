/**
 * 
 */
package exa.com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import exa.com.models.Employee;

/**
 * @author Ravikiran J
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	
	public Employee findByName(String name);
}
