/**
 * 
 */
package exa.com.app;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Ravikiran J
 *
 */

public class HelloController {

	@RequestMapping("/")
	public String showForm() {
		return "welcome";
	}

}
