/**
 * 
 */
package exa.com.topics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

/**
 * @author Ravikiran J
 *
 */
@Service
public class TopicService {


	List<Topic> topics=new ArrayList<>(Arrays.asList(
			new Topic("Spring", "Spring Framework", "Spring Descrption"),
			new Topic("Java", "Java Framework", "Java Descrption"),
			new Topic("Boot", "Boot Framework", "Boot Descrption")
			));

	public List<Topic> getAllTopics(){
		return topics;
	}

	public Topic getTopic(String id) {
		return topics.stream().filter(ddd ->ddd.getId().equals(id)).findFirst().get();

	}

	public void addtopic(Topic topic) {
		topics.add(topic);
	}

	public void update(Topic topic, String id) {
		for(int i=0;i < topics.size();i++) {
			Topic t=topics.get(i);
			if(t.getId().equals(id)) {
				topics.set(i, topic);
				return;
			}
		}

	}

}
